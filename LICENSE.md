## Llicència pel text

![Llicència de Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

Aquesta obra està subjecta a una llicència de [Reconeixement-CompartirIgual 4.0
Internacional de Creative Commons](http://creativecommons.org/licenses/by-sa/4.0/)

Per reutilitzar parts d'aquest projecte heu de seguir les indicacions següents:

**Atribució**

Heu de proporcionar crèdit als autors incloent un enllaç a la URL original
d'aquest projecte, o a una còpia estable i de lliure accés, i que contingui la
informació completa de l'autoria tal com apareix aquí.

Com que tot aquest projecte està hostatjat en un repositori de Git, al propi
registre consta l'autoria de cadascuna de les parts i en quina mesura hi ha
contribuït cadascun.

**Compartir igual**

Si feu modificacions o afegitons a la pàgina que reutilitzeu, heu de
llicenciar-los sota la llicència Reconeixement-CompartirIgual de Creative
Commons o posterior.

**Indicar els canvis**

Si en feu modificacions o afegitons, heu d'indicar que l'obra original ha estat
modificada. En una wiki o un repositori de Git la informació històrica que
proporciona el sistema és suficient.

**Nota de llicència**

Cada còpia o versió modificada que redistribuïu ha d'incloure una nota de
llicència indicant que l'obra s'ha alliberat sota la CC-BY-SA i un enllaç a
la llicència original, o bé el text complet de la llicència.

## Llicència pel codi

També podeu optar per utilitzar les parts de codi que hi ha en els vostres
propis projectes sota els termes de la llicència GPL de GNU.

Els termes exactes són aquests:

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
