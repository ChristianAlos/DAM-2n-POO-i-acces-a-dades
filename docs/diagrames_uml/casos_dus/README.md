Casos d'ús
----------

Els **casos d'ús** s'utilitzen per entendre els requeriments funcionals d'un
sistema. Un cas d'ús descriu com un usuari interacciona amb el sistema,
habitualment amb un objectiu concret. Per exemple, un usuari vol fer una
compra a través de la nostra pàgina web de venta online.

Un cas d'ús es desgrana en una sèrie de passos. Per exemple, en el cas de
la compra online, l'usuari haurà d'afegir una sèrie d'ítems al carretó,
indicar on se li han d'enviar i el mètode de pagament, i realitzar aquest
pagament. A més, aquest mateix cas d'ús inclou variacions en els possibles
escenaris que es poden donar: si l'usuari ja està registrat o no al sistema,
si el pagament es pot validar immediatament o es produeix un error, etc.

El cas d'ús agrupa tots aquests possibles escenaris i passos, perquè el seu
objectiu es entendre quin tipus d'interaccions hi haurà en el nostre sistema
i no considerar totes les possibles situacions que es poden donar.

Els **diagrames de casos d'ús** s'utilitzen per modelar els casos d'ús i
s'acostumen a realitzar en una fase inicial del projecte. Com que la seva
interpretació és senzilla, els diagrames de casos són útils per acordar amb
el client quines són les funcionalitats principals del programa. En aquest
sentit, cal evitar detallar massa els passos de cada interacció.

Els elements que actuen amb el nostre sistema s'anomenen **actors**. Hem parlat
d'usuaris, que són els actors més habituals, però n'hi ha d'altres, com
treballadors de l'empresa, administradors, o fins i tot altres sistemes
informàtics externs al nostre. Així, un actor es correspon amb un *rol* que un
usuari pren en el sistema (podria ser que el mateix usuari tingués diversos
rols, i a efectes del diagrama es representaria com a dos actors diferents).

## Contingut dels casos d'ús

Tot i l'existència dels diagrames de casos d'ús en UML, la descripció completa
del cas d'ús acostuma a ser molt més útil que el propi diagrama.

Per descriure un cas d'ús, un mètode habitual és partir d'un dels escenaris,
el que considerem l'**escenari exitós principal**. Aquest escenari es descriu
amb una sèrie de passos numerats, que indiquen cadascuna de les fases del
procés que es desenvolupa entre els actors i el sistema fins a assolir
l'objectiu del cas d'ús.

Cada un d'aquests passos ha de ser una interacció entre un actor i el sistema,
s'ha de poder expressar clarament en una sentència, i ha de deixar clar qui
està realitzant l'acció.

A continuació es descriuen les diverses variants que es poden donar de
l'escenari principal.

En alguns casos, un cas d'ús pot incloure altres casos. Per exemple, en una
botiga virtual, el cas d'ús de realitzar una compra inclou en un dels seus
passos inicials el cas d'ús de fer una cerca en el catàleg de la tenda.

Altres informacions que pot ser útil incloure a la descripció textual d'un
cas d'ús són:

- **Prerequisits**: coses que el sistema ha d'assegurar que són certes abans de
poder procedir amb el cas d'ús.
- **Garanties**: coses que el sistema ha de garantir que siguin certes un cop
el cas d'ús s'hagi finalitzat.
- **Disparador** (*trigger*): l'esdeveniment que provoca l'inici del cas d'ús.

---

[Exemple de cas d'ús](exemple_cas_dus.md)
[Exercicis](exercicis.md)
