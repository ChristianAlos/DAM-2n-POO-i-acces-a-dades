Diagrames de classes
--------------------

Els **diagrames de classes** són els diagrames UML més utilitzats, i els que
permeten un rang més ampli de conceptes de modelat. Un diagrama de
classes descriu els tipus d'objecte que apareixen en un sistema i les
diverses relacions estàtiques que hi ha entre ells. També s'hi mostren
les propietats i operacions de les classes i les restriccions que
s'apliquen a la forma com es connecten els objectes.

Els diagrames de classes són similars als diagrames entitat-relació, però
amb les següents diferències:

* Els diagrames E-R modelen només dades. En canvi els diagrames de classes
modelen tant els atributs com els mètodes de les classes. En general, de fet,
l'atenció se centra més en els mètodes que no pas en les dades.

* Els diagrames de classes permeten alguns elements que els E-R no permeten.
Per exemple, el concepte d'interfície o classe abstracte no té sentit en un
diagrama E-R.

* Els diagrames de classes poden mostrar relacions entre classes que no es
poden mostrar en E-R. Per exemple, el fet que un mètode d'una classe rebi
com a paràmetre un objecte d'una altra classe, i per tant, en depengui, és
quelcom que no es pot expressar en E-R. Això té sentit, ja que es tracta d'una
relació dinàmica i no quedaria plasmada en una base de dades.

- - -

* [Classes i interfícies](classes_interficies.md)
* [Característiques](caracteristiques.md)
* [Generalitzacions](generalitzacions.md)
* [Anotacions](anotacions.md)
* [Dependències](dependencies.md)
* [Interfícies i classes abstractes](abstractes.md)
* [Classes associatives](associatives.md)
* [Exercicis](exercicis.md)
