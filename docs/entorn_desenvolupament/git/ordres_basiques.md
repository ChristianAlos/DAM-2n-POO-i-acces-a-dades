## 10 ordres bàsiques

```
$ git add <fitxers/directori>
```

Afegeix fitxers per tal que el git en faci el seguiment, o marca fitxers
modificats per tal que siguin inclosos al següent commit.

Noteu que *git add* crea una còpia temporal del fitxer al repositori, de
manera que si tornem a modificar-lo podem optar per afegir de nou el fitxer o
recuperar la versió que havíem afegit prèviament.

*git add* és útil per indicar exactament quins fitxers volem incloure al
següent *commit* i per fer còpies temporals d'alguns fitxers.

```
$ git commit -m "missatge"
```

Crea un nou *commit* al repositori amb tots els fitxers que s'havien afegit
anteriorment i afegeix el missatge indicat al registre. Si no especifiquem
l'opció *-m* es llançarà un editor de textos per tal d'escriure el missatge.

Si inclouem l'opció *-a* afegirà tots els fitxers modificats i farà el
*commit* en un sol pas.

L'opció *--amend* ens permet modificar l'últim *commit* si ens hem
equivocat.

```
$ git status
```

Ordre bàsica per veure l'estat del directori de treball. Ens dirà quins
fitxers s'han modificat i quins s'han preparat per cometre. També ens
dóna suggeriments sobre diverses operacions habituals que podem fer.

```
$ git log
```

Permet veure el registre de canvis que s'han anat incorporant al repositori.

```
$ git diff
```

Serveix per visualitzar les diferències entre el directori de treball i
l'última versió que s'ha guardat al git.

Amb *--staged* veiem els canvis entre els fitxers que hem preparat amb *add*
i l'últim *commit*.

```
$ git rm
```

Elimina fitxers i a més marca l'eliminació com a canvi pel següent *commit*.

```
$ git mv
```

Mou un fitxer i a més marca el canvi per al següent *commit*.

```
$ git checkout -- <fitxers>
```

Descarta els canvis que haguem fet als fitxers indicats, recuperant
l'última versió que estigui guardada al repositori.

És important destacar que els canvis que estem esborrant es perden
definitivament, perquè mai han arribat a guardar-se al repositori.

```
$ git reset HEAD <fitxers>
```

És l'operació contrària a *git add*. Si hem afegit fitxers per accident
pel següent *commit*, els podem tornar a treure amb aquesta ordre.

```
$ git help <ordre>
```

Obté la pàgina de manual corresponent a l'ordre indicada.
