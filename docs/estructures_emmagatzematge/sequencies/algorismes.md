#### Algorismes: les classes Arrays i Collections

Recuperem el mètode *max* que hem creat a la secció sobre els iteradors.
A Java tenim mètodes d'aquest tipus que encara són més generals. En
comptes de tenir un mètode *max* per *Integer*, i haver-ne de fer un de
diferent per a cada tipus de dada, ens ho han fet de manera que els
podem utilitzar per a qualsevol cosa que es pugui comparar amb elements
del mateix tipus. Els tenim a la classe *Collections*:

`public static <T extends Object & Comparable<? super T>> T max(Collection<? extends T> coll)`

Fixeu-vos quina declaració tan complexa!

Independentment que no entenguem aquesta declaració, l'ús del mètode
queda clar: li passem una col·lecció de qualsevol tipus, que contingui
elements de qualsevol tipus, i ell ens retorna l'element més gran que hi
ha a la col·lecció. L'única restricció és que els elements de la
col·lecció es puguin comparar entre ells, ja que si no, no té sentit
parlar del seu màxim. Així, ho podrem fer servir per col·leccions que
continguin *Integer*, *Double*, *String*... i qualsevol altra classe que
implementi la interfície *Comparable*. Aquesta interfície s'utilitza per
indicar que els elements d'una certa classe es poden comparar, i la
veurem amb detall al tema de programació orientada a objectes.

La classe *Collections* conté una sèrie de mètodes estàtics útils per
treballar amb col·leccions. Per exemple, tenim el mètode *sort* per
ordenar una col·lecció, el mètode *swap* per intercanviar dos elements
de la col·lecció, el *shuffle* per barrejar la col·lecció i el *copy*
per copiar una col·lecció, entre d'altres.

La classe *Arrays* és similar, però conté mètodes útils per treballar
amb vectors estàtics, algun dels quals ja ha anat apareixent al llarg
dels exemples del curs.
