package clases;

public class Persona implements Comparable<Persona> {
	float pes;
	int edad;
	float alcada;
	
	public Persona(float pes, int edad, float alt){
		super();
		this.pes = pes;
		this.edad = edad;
		this.alcada = alt;
	}

	@Override
	public int compareTo(Persona p) {
		
		if (this.edad > p.edad)
			return 1;
		else if (this.edad < p.edad)
			return -1;
		else
			return 0;
	}

	public float getPes() {
		return pes;
	}

	public void setPes(float pes) {
		this.pes = pes;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public float getAlcada() {
		return alcada;
	}

	public void setAlcada(float alcada) {
		this.alcada = alcada;
	}
}
