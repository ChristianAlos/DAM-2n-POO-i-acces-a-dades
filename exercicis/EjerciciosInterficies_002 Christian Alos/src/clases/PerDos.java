package clases;



public class PerDos implements Serie{
	double num;
	public PerDos(){
		
	}

	@Override
	public void enSeriar(double n) {
		this.num = n;		
	}

	@Override
	public void enserieDefecto() {
		enSeriar(1);
	}

	@Override
	public double serializar() {
 		return this.num * 2;
	}

	public double getNum() {
		return num;
	}

	public void setNum(double num) {
		this.num = num;
	}



}
