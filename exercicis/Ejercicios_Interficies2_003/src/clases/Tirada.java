package clases;


import java.util.Arrays;
import java.util.Random;

public class Tirada implements Comparable<Tirada>{
	
	public int atac;
	public int defensa;
	public int numDados;
	static final Random r = new Random(System.currentTimeMillis());
	public int[] tiros;
	private int[] newTiros;
	private StringBuffer jugador1;
	
	public Tirada (int a, int d, int nd){
		this.atac = a;
		this.defensa = d;
		this.numDados = nd;
		tiros = new int[nd];
		for(int i = 0; i < nd; i++){
			tiros[i] = r.nextInt(6) + 1;
		}
		
	}

	@Override
	public String toString() {
		StringBuffer cadena = new StringBuffer();
		Arrays.sort(tiros);
		for (int i = 0; i < tiros.length; i++){
			cadena =cadena.append(tiros[i]);
		}
		return "AT: " + atac + " DEF: " + defensa + " Daus(" + numDados +"): " + cadena; 
	}

	@Override
	public int compareTo(Tirada t) {
		jugador1 = new StringBuffer();

		if (this.tiros.length > t.tiros.length){
			t.tiros = tirada (t.tiros, this.tiros);
			
		}else if (this.tiros.length < t.tiros.length){
			this.tiros = tirada (this.tiros, t.tiros);
			
		}

		Arrays.sort(t.tiros);
		Arrays.sort(this.tiros);
		
		
		
		int ataque = 0;
		int defensa = 0;
		int resultado1 = 0;
		int resultado2 = 0;
		//ataque this
		for (int i = 0; i < this.tiros.length; i++){
			ataque = this.atac + this.tiros[i];
			defensa = t.defensa + t.tiros[i];
			if (ataque > defensa){
				resultado1 = resultado1 + 1;
			}
		}
		//ataque t
		for (int i = 0; i < this.tiros.length; i++){
			defensa = this.defensa + this.tiros[i];
			ataque = t.atac + t.tiros[i];
			if (ataque > defensa){
				resultado2 = resultado2 + 1;
				
			}
		}
		if (resultado1 > resultado2){
			return 1;
		}else if (resultado1 < resultado2){
			return -1;
		}else{
			return 0;
		}
		
	}

	public int[] tirada (int [] varMenor, int [] t2){
		newTiros = varMenor;
		varMenor = new int[t2.length];

		for (int i = 0; i < t2.length;i++){
			if (i < newTiros.length){
				varMenor[i] = newTiros[i];
			}else{
				varMenor[i] = 0;
			}
		}
		return varMenor;
	}
	/*
	newTiros = t.tiros;
	t.tiros = new int[this.tiros.length];
	
	//for (int i: t.tiros) System.out.println(i);
	for (int i = 0; i < this.tiros.length;i++){
		if (i < newTiros.length){
			t.tiros[i] = newTiros[i];
		}else{
			t.tiros[i] = 0;
		}
	}
	*/
}
