package clases;

public interface Listener {
	public void notifyEvent(int n);
}
