package clases;

import java.util.ArrayList;

public class ProdueixEsdeveniments{
	
	ArrayList<Listener> llistaEsdeveniments = new ArrayList<>();
	
	public void addEventListener (Listener n){
		llistaEsdeveniments.add(n);
	}
	public void creaEsdeveniment(int numEsdeveniment){
		for (Listener i: llistaEsdeveniments){
			i.notifyEvent(numEsdeveniment);
		}
		
	}

	
}
