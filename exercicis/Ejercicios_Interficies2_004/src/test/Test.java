package test;

import clases.GuardarEsdeveniments;
import clases.MostraEsdeveniments;
import clases.ProdueixEsdeveniments;

public class Test {

	public static void main(String[] args) {
		GuardarEsdeveniments ge = new GuardarEsdeveniments();
		MostraEsdeveniments me = new MostraEsdeveniments();
		ProdueixEsdeveniments pe = new ProdueixEsdeveniments();
		pe.addEventListener(ge);
		pe.addEventListener(me);
		
		pe.creaEsdeveniment(3);
		pe.creaEsdeveniment(6);
		pe.creaEsdeveniment(2);
		System.out.println(ge.toString());
		
	}

}
