package Clases;


public class Jardi{
	Planta jardi[];
	Integer posicion = 0;
	Planta p;

	public Jardi(int mida){
		if(mida <= 0){
			mida = 10;
		}
		this.jardi = new Planta[mida];

	}

	public void temps(){

		for(int i = 0; i < this.jardi.length; i++){
			if(this.jardi[i] != null){
				p = this.jardi[i].creix();

				if (p instanceof Llavor){
					posicion = this.jardi[i].escampaLlavor();
					if((posicion + i) >= 0 && (posicion + i) < this.jardi.length){
						//TODO posicion libre
						this.jardi[posicion + i] = p;
						plantaLlavor(p, posicion + i);
					}

				}else if (p  instanceof Planta){
					this.jardi[i] = p;
				}else if (this.jardi[i].isEsViva() == false){
					this.jardi[i] = null;
				}
			}
		}
	}
	public boolean plantaLlavor(Planta novaPlanta, int pos){
		System.out.println(""+pos);
		if (this.jardi[pos] == null){
			this.jardi[pos] = novaPlanta;
			return true;
		}else{
			return false;
		}
	}
	@Override
	public String toString() {

		String s = new String();
		for(int i = this.jardi.length; i > 0 ; i--){
			for (int j = 0; j < this.jardi.length; j++){
				if(this.jardi[j] != null){
					s = s + this.jardi[j].getChar(i);
				}else{
					s = s + " ";
				}
			}
			s = s + "\n";
		}
		for (int i = 0; i < this.jardi.length; i++){
			s = s + "-";
		}


		return s;
	}
}
