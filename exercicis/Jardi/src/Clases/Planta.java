package Clases;

import java.util.Random;

public abstract class Planta{
	public boolean esViva = true;
	protected int altura;
	char tallo;
	char flor;
	int posSemilla;
	int min = -2;
	int max = 2;
	
	public Planta creix(){
		
		if (altura < 10){
			this.altura = this.altura + 1;
		}else if (altura == 10){
			this.esViva = false;
		}
		
		return null;	
	}
	int escampaLlavor(){
		Random random = new Random();
		int i = 0;
		while (i == 0){
			i = random.nextInt(5)-2;
			/*
			posSemilla = (int) (Math.random()*(max - (min) + 1)+min);
			*/
			System.out.println("NUMERO: " + i);
		}
		return i;
		
		
	}
	public int getAltura() {
		return altura;
	}
	public boolean isEsViva() {
		return esViva;
	}

	public char getChar(int nivell) {
		if(nivell > this.altura){
			return ' ';
		}else if (nivell < this.altura){
			return this.tallo;
		}else if (nivell == this.altura){
			return this.flor;
		}else{
			//poner excepcion si es menor que 0 el nivel
			return 'N';
		}
	}
	
}
